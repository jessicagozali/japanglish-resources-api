# Japanglish Resources Rails API

## Purpose

Japanglish Resources Rails API serves information about links to Youtube in JSON format that are displayed on Resources view: [https://japanglish.jessicagozali.com.au/resources](https://japanglish.jessicagozali.com.au/resources)

&nbsp;    

---

&nbsp;    

## Data Structure

Data served from the API in JSON format, contains following information in the below structure:

```
{
  refUrl: "link to Youtube video",
  imgUrl: "link for Youtube thumbnail picture",
  title: "title of the video",
  description: "short explanation about the video"
}
```

&nbsp;    

---

&nbsp;    

## API Endpoint

The Japanglish app would be able to access the below endpoint:

```
https://japanglish-resources-api.herokuapp.com/api/resources
```

&nbsp;    

---

&nbsp;    

## Tech Stack

This API was build using Rails framework with PostgreSQL database, deployed on Heroku.

&nbsp;    

---

&nbsp;    

## Acknowledgements

All resources listed belongs to respective material owner, obtained through Youtube sharing options.
