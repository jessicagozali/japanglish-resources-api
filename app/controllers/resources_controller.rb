class ResourcesController < ApplicationController
  before_action :set_resource, only: [:show, :update, :destroy]

  def index
    @resources = Resource.all
    render json: @resources
  end

  def create
    @resource = Resource.create(resource_params)

    if @resource.errors.any?
      render json: @resource.errors, status: :unprocessable_entity
    else
      render json: @resource, status: 201
    end
  end
  
  def show
    render json: @resource, status: 200
  end

  def update
    @resouce.update(resource_params)

    if @resource.errors.any?
      render json: @resource.errors, status: :unprocessable_entity
    else
      render json: @resource, status: 201
    end
  end

  def destroy
    @resouce.delete
    render json: 204
  end

  private
  def resource_params
    params.require(:resource).permit(:refUrl, :imgUrl, :title, :description)
  end

  def set_resource
    begin
      @resource = Resource.find(params[:id])
    rescue
      render json: {error: "Resource not found."}, status: 404
    end
  end
end
