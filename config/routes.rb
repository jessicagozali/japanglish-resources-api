Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  scope '/api' do
    get '/resources', to: 'resources#index'
    post '/resources/new', to: 'resources#create'
    get '/resources/:id', to: 'resources#show'
    put '/resources/:id', to: 'resources#update'
    delete '/resources/:id', to: 'resources#destroy'
  end
  root to: 'resources#index'
end
