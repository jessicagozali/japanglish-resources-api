# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Resource.create(refUrl: "https://youtu.be/zhGnuWwpNxI", imgUrl: "https://img.youtube.com/vi/zhGnuWwpNxI/0.jpg", title: "Funny Japanglish Song for Tokyo Olympic!【Tokyo Bon 東京盆踊り】Namewee黃明志 Ft. Meu Ninomiya & Cool Japan TV", description: "A very popular song teaching a lot of Japanglish that you would commonly hear or use in Japan. You might befriend a Japanese if you sing this song in public.")

Resource.create(refUrl: "https://youtu.be/mJSozbYKXJk", imgUrl: "https://img.youtube.com/vi/mJSozbYKXJk/0.jpg", title: "歩きスマホ(Aruki Sumaho)- Walking and Texting", description: "A video by Saori in Sapporo explaning what Aruki Sumaho is and demonstrations of what sort of consequences that may happen when someone decides to carry the act in Japan.")

Resource.create(refUrl: "https://youtu.be/7I2Ryji_9Js", imgUrl: "https://img.youtube.com/vi/7I2Ryji_9Js/0.jpg", title: "【MV】『Song for Learning Japanese』 Mihara Keigo（三原慧悟", description: "A cute video, not directly related to learning Japanglish, but more like learning Japanese language in general. Repeat it until it sticks in your mind.")

Resource.create(refUrl: "https://youtu.be/Yv6shy_9KVM", imgUrl: "https://img.youtube.com/vi/Yv6shy_9KVM/0.jpg", title: "打首獄門同好会 (Uchikubi Gokumon Doukoukai) - I don' t wanna get out of futon", description: "How did this video link get up here, you ask? Well, because it's cute! What else?! Good for learning Japanese in general.")

Resource.create(refUrl: "https://youtu.be/_HDwg93Ti4o?t=133", imgUrl: "https://img.youtube.com/vi/_HDwg93Ti4o/0.jpg", title: "Pekora plays an English Quiz, makes a lot of sound effects and laughs at every question", description: "Check out Pekora doing an English Quiz to get a glimps of how English is taught in Japan. We might get a clue of how Japanglish was born.")

puts "Create resources"
