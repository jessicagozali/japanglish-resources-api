class CreateResources < ActiveRecord::Migration[6.1]
  def change
    create_table :resources do |t|
      t.string :refUrl
      t.string :imgUrl
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
